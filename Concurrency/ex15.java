// Create a class with three methods containing critical sections
// that all Sync1hronize on the same object. Create multiple tasks to
// demonstrate that only one of these methods can run at a time. Now
// modify the methods so that each one Sync1hronizes on a different 
// object and show that all three methods can be runing at once. 

class Sync1 { // all methods synchronized on this
	public void f1() {
		synchronized(this) {
			for(int i = 0; i < 5; i++) {
				System.out.println("f1()");
				Thread.yield();
			}
		}
	}
	public void g1() {
		synchronized(this) {
			for(int i = 0; i < 5; i++) {
				System.out.println("g1()");
				Thread.yield();
			}
		}
	}
	public void h1() {
		synchronized(this) {
			for(int i = 0; i < 5; i++) {
				System.out.println("h1()");
				Thread.yield();
			}
		}
	}
}

class Sync2 {
	private Object SynchObject1 = new Object();
	private Object SynchObject2 = new Object();
	private Object SynchObject3 = new Object();
	public void f2() {
		synchronized(SynchObject1) {
			for(int i = 0; i < 5; i++) {
				System.out.println("f2()");
				Thread.yield();
			}
		}
	}
	public void g2() {
		synchronized(SynchObject2) {
			for(int i = 0; i < 5; i++) {
				System.out.println("g2()");
				Thread.yield();
			}
		}
	}
	public void h2() {
		synchronized(SynchObject3) {
			for(int i = 0; i < 5; i++) {
				System.out.println("h2()");
				Thread.yield();
			}
		}
	}
}

public class ex15 {
	public static void main(String[] args) {
		final Sync1 st1 = new Sync1();
		final Sync2 st2 = new Sync2();
		new Thread() {
			public void run() {
				st1.f1();
			}
		}.start();
		new Thread() {
			public void run() {
				st1.g1();
			}
		}.start();
		new Thread() {
			public void run() {
				st1.h1();
			}
		}.start();		
		new Thread() {
			public void run() {
				st2.f2();
			}
		}.start();
		new Thread() {
			public void run() {
				st2.g2();
			}
		}.start();
		new Thread() {
			public void run() {
				st2.h2();
			}
		}.start();			
	}
}
