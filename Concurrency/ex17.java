// Create a radiation counter that can have any number of remote sensors.

import java.util.concurrent.*;
import java.util.*;

class RadCounter {
	private int count = 0;
	private Random rand = new Random();
	public synchronized int increment() {
		return count++;
	}
	public synchronized int value() { return count; } 
}

class Sensor implements Runnable {
	private static RadCounter count = new RadCounter();
	private static List<Sensor> sensors = new ArrayList<Sensor>();
	private int number = 0;
	private final int id;
	private static volatile boolean stop = false;
	public static void cancel() { stop = true; }
	public Sensor(int id) {
		this.id = id;
		sensors.add(this);
	}
	public void run() {
		while(!stop) {
			synchronized(this) {
				number++;
			}
			System.out.println(this + " Total: " + count.increment());
			try {
				TimeUnit.MILLISECONDS.sleep(25);
			} catch(InterruptedException e) {
				System.out.println("sleep interrupted");
			}
		}
		System.out.println("Stopping " + this);
	}
	public synchronized int getValue() { return number; }
	public String toString() {
		return "Sensor " + id + ": " + getValue();
	} 
	public static int getTotalCount() {
		return count.value();
	}
	public static int sumSensors() {
		int sum = 0;
		for(Sensor sensor : sensors)
			sum += sensor.getValue();
		return sum;
	}
}

public class ex17 {
	public static void main(String[] args) throws Exception {
		ExecutorService exec = Executors.newCachedThreadPool();
		for(int i = 0; i < 10; i++)
			exec.execute(new Sensor(i));
		// Run for a while, then stop and collect the data:
		TimeUnit.SECONDS.sleep(4);
		Sensor.cancel();
		exec.shutdown();
		if(!exec.awaitTermination(250, TimeUnit.MILLISECONDS))
			System.out.println("Some tasks were not terminated");
		System.out.println("Total: " + Sensor.getTotalCount());
		System.out.println("Sum of Sensors: " + Sensor.sumSensors());
	}
}