// Repair AtomicityTest.java using the synchronized keyword. 
// Can you demonstrate that it is now correct?

import java.util.concurrent.*;

class AtomicityTest implements Runnable {
	private int i = 0;
	public synchronized int getValue() { return i; }
	private synchronized void evenIncrement() { i++; i++; }
	public void run() {
		while(true) {
			evenIncrement();
		}
	}
}

public class ex12 {
	public static void main(String[] args) {
		ExecutorService exec = Executors.newCachedThreadPool();
		AtomicityTest at = new AtomicityTest();
		exec.execute(at);
		while(true) {
			int val = at.getValue();
			if(val % 2 != 0) { // i is always even, so program never stops
				System.out.println(val);
				System.exit(0);
			}
		}
	}
}