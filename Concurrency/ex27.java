// Modify Restaurant.java to use explicit Lock and Condition objects.

import java.util.concurrent.*;
import java.util.concurrent.locks.*;

class Meal {
	private final int orderNum;
	public Meal(int orderNum) { this.orderNum = orderNum; }
	public String toString() { return "Meal " + orderNum; }
}

class WaitPerson implements Runnable {
	private Restaurant restaurant;
	protected Lock lock = new ReentrantLock();
	protected Condition condition = lock.newCondition();
	public WaitPerson(Restaurant r) { restaurant = r; }
	public void run() {
		try {
			while(!Thread.interrupted()) {
				lock.lock();
				try {
					while(restaurant.meal == null)
						condition.await();
				} finally {
					lock.unlock();
				}
				System.out.println("waitPerson got " + restaurant.meal);
				restaurant.chef.lock.lock();
				try {
					restaurant.meal = null;
					restaurant.chef.condition.signalAll();
				} finally {
					restaurant.chef.lock.unlock();
				}				
			}	
		} catch(InterruptedException e) {
			System.out.println("WaitPerson interrupted");
		}
	}
}

class Chef implements Runnable {
	private Restaurant restaurant;
	private int count = 0;
	protected Lock lock = new ReentrantLock();
	protected Condition condition = lock.newCondition();
	public Chef(Restaurant r) { restaurant = r; }
	public void run() {
		try {
			while(!Thread.interrupted()) {
				lock.lock(); 
				try {
					while(restaurant.meal != null)
						condition.await();
				} finally {
					lock.unlock();
				}
				if(++count == 10) {
					System.out.println("Out of food, closing");
					restaurant.exec.shutdownNow();
					return;
				}
				System.out.println("Order up! ");
				restaurant.waitPerson.lock.lock();
				try {
					restaurant.meal = new Meal(count);
					restaurant.waitPerson.condition.signalAll();
				} finally {
					restaurant.waitPerson.lock.unlock();
				}
				TimeUnit.MILLISECONDS.sleep(100);
			}
		} catch(InterruptedException e) {
			System.out.println("chef interrupted");
		}
	}	
}

class Restaurant {
	Meal meal;
	ExecutorService exec = Executors.newCachedThreadPool();
	WaitPerson waitPerson = new WaitPerson(this);
	Chef chef = new Chef(this);
	public Restaurant() {
		exec.execute(chef);
		exec.execute(waitPerson);
	}
}

public class ex27 {
	public static void main(String[] args) {
		new Restaurant();
	}
}