// Demonstrate that java.util.Timer scales to large numbers by 
// creating a program that generates many Timer objects that perform
// some simple task when the timeout completes.

import java.util.*;
import java.util.concurrent.*;

class Demo implements Runnable {
	private static int timers = 0;
	private static int tasks = 0;
	public void run() {
		try {
			while(timers < 10000) {
				timers++;
				Timer t = new Timer();		
				// declaring taks as anonymous classes
				t.schedule(new TimerTask() {
					public void run() {
						tasks++; 
						if(timers % 100 == 0)		
							System.out.println(timers + " timers did " + tasks + " tasks");
					}
				}, 0); // delay is 0
				try {
					TimeUnit.MILLISECONDS.sleep(5); // time to do task
				} catch(InterruptedException e) {
					System.out.println("Sleep interrupted");
				}
				t.cancel();
			}
		} finally {
			System.out.println("Done. " + timers + " timers completed " + tasks + " tasks");
		} 
	}
}

public class ex14 {
	public static void main(String[] args) {
		ExecutorService exec = Executors.newCachedThreadPool();
		exec.execute(new Demo());
		exec.shutdown();
	}
}