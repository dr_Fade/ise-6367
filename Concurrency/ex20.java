// Modify CachedThreadPool so that all threads receive an interrupt()
// before they are completed.

import java.util.concurrent.*;

class LiftOff implements Runnable {
	protected int countDown = 10; // Default
	private static int taskCount = 0;
	private final int id = taskCount++;
	public LiftOff() {}
	public LiftOff(int countDown) {
		this.countDown = countDown;
	}
	public String status() {
		return "#" + id + "(" + (countDown > 0 ? countDown : "Liftoff!") + "), ";
	}

	public void run() {
		while(countDown-- > 0) {
		System.out.print(status());
		Thread.yield();
		}
	}
}

public class ex20 {
	public static void main(String[] args) throws Exception {
		System.out.println("Using LiftOff:");
		ExecutorService exec = Executors.newCachedThreadPool();
		for(int i = 0; i < 5; i++) {
			Future<?> f = exec.submit(new LiftOff());
			f.cancel(true);							
		}
		exec.shutdownNow();
		if(!exec.awaitTermination(250, TimeUnit.MILLISECONDS))
			System.out.println("Some tasks were not terminated");
		// Using modified LiftOff:
		System.out.println("\nUsing LiftOff20:"); 
		ExecutorService exec20 = Executors.newCachedThreadPool();
		for(int i = 0; i < 5; i++) {
			Future<?> f = exec20.submit(new LiftOff());
			f.cancel(true);					
		}
		exec20.shutdownNow();
		if(!exec.awaitTermination(250, TimeUnit.MILLISECONDS))
			System.out.println("Some tasks were not terminated");		
	}
}