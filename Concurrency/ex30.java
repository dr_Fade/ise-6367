// Modify PipedIO.java to use a BlockingQueue instead of a pipe.

import java.util.concurrent.*;
import java.io.*;
import java.util.*;

class Sender implements Runnable {
	private Random rand = new Random(47);
	private LinkedBlockingQueue<Character> queue;
	public Sender(LinkedBlockingQueue<Character> lbq) {
		queue = lbq;
	}
	public void run() {
		try {
			while(true)
				for(char c = 'A'; c <= 'z'; c++) {
					queue.put(c);
					TimeUnit.MILLISECONDS.sleep(250);
				}
		} catch(InterruptedException e) {
			System.out.println(e + " Sender sleep interrupted");
		}
	}
}

class Receiver implements Runnable {
	private LinkedBlockingQueue<Character> queue;
	public Receiver(LinkedBlockingQueue<Character> lbq) {
		queue = lbq;
	}
	public void run() {
		 try {
			while(true) {
				// Blocks until characters are there:
				System.out.println("Read: " + (char)queue.take() + ", ");
			}
		 } catch(InterruptedException e) {
			System.out.println(e + " Receiver read exception");
		}
	}
}

public class ex30 {
	public static void main(String[] args) throws Exception {
		LinkedBlockingQueue<Character> lbq = new LinkedBlockingQueue<Character>();
		Sender sender = new Sender(lbq);
		Receiver receiver = new Receiver(lbq);
		ExecutorService exec = Executors.newCachedThreadPool();
		exec.execute(sender);
		exec.execute(receiver);
		TimeUnit.SECONDS.sleep(10);
		exec.shutdownNow();
	}
}