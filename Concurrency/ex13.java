// Repair SerialNumberChecker.java using the synchronized keyword.
// Can you demonstrate that it is now correct?
// { Args: 10 }

import java.util.concurrent.*;

class SerialNumberGenerator {	
	private static volatile int serialNumber = 0;
	public static synchronized int nextSerialNumber() { // synchronized!
		return serialNumber++; // Not thread-safe
	}	
}

// Reuses storage so we don't run out of memory:
class CircularSet {
	private int[] array;
	private int len;
	private int index = 0;
	public CircularSet(int size) {
		array = new int[size];
		len = size;
		// Initialize to a value not produced
		// by the SerialNumberGenerator:
		for(int i = 0; i < size; i++)
			array[i] = -1;
	}
	public synchronized void add(int i) {
		array[index] = i;
		// Wrap index and write over old elements:
		index = ++index % len;
	}
	public synchronized boolean contains(int val) {
		for(int i = 0; i < len; i++)
			if(array[i] == val) return true;
		return false;
	}
}

class SerialNumberChecker {	
	public static final int SIZE = 10;
	public static ExecutorService exec = Executors.newCachedThreadPool();
	private static CircularSet serials = new CircularSet(1000);
	static class SerialChecker implements Runnable {
		public void run() {
			while(true) {
				int serial = SerialNumberGenerator.nextSerialNumber();
				if(serials.contains(serial)) {
					System.out.println("Duplicate: " + serial);
					System.exit(0);
				}
				serials.add(serial);
			}
		}
	}
}

public class ex13 {
	public static void main(String[] args) throws Exception {
		for(int i = 0; i < SerialNumberChecker.SIZE; i++)
			SerialNumberChecker.exec.execute(new SerialNumberChecker.SerialChecker());
		// Stop after n seconds if there's an argument:
		if(args.length > 0) {
			TimeUnit.SECONDS.sleep(new Integer(args[0]));
			System.out.println("No duplicates detected");
			System.exit(0);
		}
	}	
}