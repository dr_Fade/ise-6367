// Add a BusBoy class to Restaurant.java. After the meal is delivered,
// the WaitPerson should notify the BusBoy to clean up. 

import java.util.concurrent.*;

class Meal {
	private final int orderNum;
	public Meal(int orderNum) { this.orderNum = orderNum; }
	public String toString() { return "Meal " + orderNum; }
}

class WaitPerson implements Runnable {
	private Restaurant restaurant;
	protected boolean clean = true;
	protected Meal m; // 
	public WaitPerson(Restaurant r) { restaurant = r; }
	public void run() {
		try {
			while(!Thread.interrupted()) {
				synchronized(this) {
					while(restaurant.meal == null)
						wait(); // ... for the chef to produce a meal
				}
				m = restaurant.meal;
				System.out.println("WaitPerson got " + m);
				synchronized(restaurant.chef) {
					restaurant.meal = null;
					restaurant.chef.notifyAll(); // ready for another
				}
				System.out.println("WaitPerson delivered " + m);
				synchronized(restaurant.busBoy) { 
					clean = false;
					restaurant.busBoy.notifyAll(); // for cleanup
				}
			}	
		} catch(InterruptedException e) {
			System.out.println("WaitPerson interrupted");
		}
	}
}

class Chef implements Runnable {
	private Restaurant restaurant;
	private int count = 0;
	public Chef(Restaurant r) { restaurant = r; }
	public void run() {
		try {
			while(!Thread.interrupted()) {
				synchronized(this) {
					while(restaurant.meal != null) 
						wait(); // ... for the meal to be taken
				}
				if(++count == 10) {
					System.out.println("Out of food, closing");
					restaurant.exec.shutdownNow();
					return;
				}
				System.out.println("Order up! ");
				synchronized(restaurant.waitPerson) {
					restaurant.meal = new Meal(count);
					restaurant.waitPerson.notifyAll();
				}
				TimeUnit.MILLISECONDS.sleep(100);
			}
		} catch(InterruptedException e) {
			System.out.println("Chef interrupted");
		}
	}	
}

class BusBoy implements Runnable {
	private Restaurant restaurant;
	public BusBoy(Restaurant r) { restaurant = r; }
	public void run() {
		try {
			while(!Thread.interrupted()) {
				synchronized(this) {
					while(restaurant.waitPerson.clean)
						wait();
				}
				System.out.println("BusBoy cleaning up " + restaurant.waitPerson.m);
				restaurant.waitPerson.clean = true;
			}
		} catch(InterruptedException e) {
			System.out.println("BusBoy interrupted");
		}
	}
}

class Restaurant {
	Meal meal;
	ExecutorService exec = Executors.newCachedThreadPool();
	WaitPerson waitPerson = new WaitPerson(this);
	BusBoy busBoy = new BusBoy(this);
	Chef chef = new Chef(this);
	public Restaurant() {
		exec.execute(chef);
		exec.execute(waitPerson);
		exec.execute(busBoy);
	}
}

public class ex26 {
	public static void main(String[] args) {
		new Restaurant();
	}
}