//: innerclasses/controller/Controller.java
// The reusable framework for control systems.
import java.util.*;

abstract class Event {
  private long eventTime;
  protected final long delayTime;
  public Event(long delayTime) {
    this.delayTime = delayTime;
    start();
  }
  public void start() {
    eventTime = System.nanoTime() + delayTime;
  }
  public boolean ready() {
    return System.nanoTime() >= eventTime;
  }
  public abstract void action();
}

class Controller {
  private LinkedList<Event> eventList = new LinkedList<Event>();
  public void addEvent(Event c) { eventList.add(c); }   
  public void run() {   
    LinkedList<Event> eventListCopy = new LinkedList<Event>(eventList);
    ListIterator<Event> it = eventListCopy.listIterator();
    while(it.hasNext()) { 
      it.next().action();
      it.previous();    
      System.out.println(it.next());    
    }
  } 
}

public class ex13{
  public static void main(String[] args) {
    new Controller().run();
  }
}