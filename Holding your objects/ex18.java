// Fill a HashMap with key-value pairs. Print the results to show ordering
// by hash code. Extract the pairs, sort by key, and place the result into a 
// LinkedHashMap. Show that the insertion order is maintained. 
import java.util.*;

class Gerbil {
	private int gerbilNumber;
	public Gerbil(int i) {
		gerbilNumber = i;
	}
	public void hop() {
		System.out.println("gerbil " + gerbilNumber + " hops");
	}
}

public class ex18 {
	public static void main(String[] args) {
		Map<String, Gerbil> gerbils = new HashMap<String, Gerbil>();
		gerbils.put("name3", new Gerbil(0));
		gerbils.put("name4", new Gerbil(1));
		gerbils.put("name1", new Gerbil(2));
		gerbils.put("name2", new Gerbil(3));
		System.out.println(gerbils.keySet());

		Set<String> sortedKeys = new TreeSet<String>(gerbils.keySet());
		
		Map<String, Gerbil> sortedGerbils = new LinkedHashMap<String, Gerbil>();
		for(String s : sortedKeys) {
			sortedGerbils.put(s, gerbils.get(s));			
		}
		System.out.println(sortedGerbils.keySet());
	}
}

