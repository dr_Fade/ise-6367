// Fill a LinkedHashMap with String keys and objects of your choice.
// Now extract the pairs, sort them based on the keys, and reinsert
// them into the Map.
import java.util.*;

public class ex24 {	
	public static void main(String[] args) {
		Map<Integer,String> m = new LinkedHashMap<Integer,String>();
		m.put(10,"ten");
		m.put(9,"nine");
		m.put(8,"eight");
		m.put(7,"seven");
		m.put(6,"six");
		m.put(5,"five");
		m.put(4,"four");
		m.put(3,"three");
		m.put(2,"two");
		m.put(1,"one");
		m.put(0,"zero");

		System.out.println(m);

		Map<Integer,String> mTemp =	new LinkedHashMap<Integer,String>();
		
		Set<Integer> ss = new TreeSet<Integer>(m.keySet());
		Iterator<Integer> itss = ss.iterator();
		
		while(itss.hasNext()) {
			Integer i = (Integer)itss.next();
			String s = m.get(i);
			m.remove(i);
			mTemp.put(i, s);
		}
		
		Set<Integer> ssTemp = new TreeSet<Integer>(mTemp.keySet());
		Iterator<Integer> itssTemp = ssTemp.iterator();
		while(itssTemp.hasNext()) {
			Integer i = (Integer)itssTemp.next();
			String s = mTemp.get(i);
			mTemp.remove(i);
			m.put(i, s);
		}
		mTemp.clear();
		System.out.println(m);
	}
}	

