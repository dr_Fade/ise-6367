// Fill a PriorityQueue (using offer()) with Double values created using 
// java.util.Random, then remove the elements using poll() and display them.
import java.util.*;

class Class extends Object {}

public class ex29 {
	public static void main(String[] args) {		
		PriorityQueue<Class> s = new PriorityQueue<Class>();
		s.offer(new Class());
		//! s.offer(new Class()); 
		// runtime error when offering another Class
	}
}


