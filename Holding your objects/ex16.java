// Create a Set of the vowels. Working from UniqueWords.java, count and
// display the number of vowels in each input word, and also display the total
// number of vowels in the input file.
import java.util.*;

public class ex16 {
	public static void main(String[] args) {
		Set<String> words = new TreeSet<String>(Arrays.asList(args));

		Set<Character> vowels = new TreeSet<Character>();
		Collections.addAll(vowels, 'A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u');
		int allVowels = 0;

		for(String s : words) {
			int count = 0;
			for(Character v : s.toCharArray()) {		
				if(vowels.contains(v)) {
					count++;
					allVowels++; 
				}
			}
			System.out.print(s + ": " + count + ", ");		
		}

		System.out.println();	
		System.out.println("Total vowels: " + allVowels);
	}
}