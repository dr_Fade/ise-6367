// Repeat the previous exercise with a HashSet and a LinkedHashSet.
import java.util.*;

class Gerbil {
	private int gerbilNumber;
	public Gerbil(int i) {
		gerbilNumber = i;
	}
	public void hop() {
		System.out.println("gerbil " + gerbilNumber + " hops");
	}
}

public class ex19 {
	public static void main(String[] args) {
		Map<String, Gerbil> gerbils = new HashMap<String, Gerbil>();
		gerbils.put("name3", new Gerbil(0));
		gerbils.put("name4", new Gerbil(1));
		gerbils.put("name1", new Gerbil(2));
		gerbils.put("name2", new Gerbil(3));
		System.out.println(gerbils.keySet());

		Set<String> sortedKeys = new TreeSet<String>(gerbils.keySet());
		Set<String> linkedHashedKeys = new LinkedHashSet<String>(sortedKeys);

		Map<String, Gerbil> linkedHashedGerbils = new LinkedHashMap<String, Gerbil>();
		for(String s : linkedHashedKeys) {
			linkedHashedGerbils.put(s, gerbils.get(s));
		}
		System.out.println(linkedHashedGerbils.keySet());
	}
}

