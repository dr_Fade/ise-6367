//: polymorphism/shape/Square.java
package shape;

public class Square extends Shape {
  public void draw() { System.out.println("Square.draw()"); }
  public void erase() { System.out.println("Square.erase()"); }
} ///:~
