// Create a generator class that produces character names (as String objects)
// from your favorite movie (you can use Snow White or Star Wars as a
// fallback) each time you call next(), and loops around to the beginning of
// the character list when it runs out of names. Use this generator to fill
// an array, an ArrayList, a LinkedList, a HashSet, a LinkedHashSet, and a
// TreeSet, then print each container.
import java.util.*;

class Generator {
	int key = 0;
	public String next() {
		switch(key) {
			default:
			case 0 : key++; return "name1";
			case 1 : key++; return "name2";
			case 2 : key++; return "name3";
			case 3 : key++; return "name4";
			case 4 : key++; return "name5";
			case 5 : key++; return "name6";
			case 6 : key++; return "name7";
			case 7 : key = 0; return "name8";			
		}
	} 
	public void fillA(String[] a) {
		for(int i = 0; i < a.length; i++)
			a[i] = next();
	}
	public Collection fill(Collection<String> c, int n) {
		for(int i = 0; i < n; i++) c.add(next());
		return c;
	}
}

public class ex4 {
	public static void main(String[] args) {
		Generator gen = new Generator();		
		String[] a = new String[8];
		gen.fillA(a);

		for(String s : a) System.out.println(s);
		System.out.println();
		System.out.println(gen.fill(new ArrayList<String>(), 8));
		System.out.println(gen.fill(new LinkedList<String>(), 8));
		System.out.println(gen.fill(new HashSet<String>(), 8));
		System.out.println(gen.fill(new LinkedHashSet<String>(), 8));
		System.out.println(gen.fill(new TreeSet<String>(), 8));
	}
}