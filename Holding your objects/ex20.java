// Modify Exercise 16 so that you keep a count of the occurence of each vowel. 
import java.util.*;

public class ex20 {
	public static void main(String[] args) {
		Set<String> words = new TreeSet<String>(Arrays.asList(args));
		
		Set<Character> vowels = new TreeSet<Character>();
		Collections.addAll(vowels, 'A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u');
		int allVowels = 0;
		Map<Character,Integer> vowelMap = new TreeMap<Character,Integer>();
		
		for(String s : words) {
			for(Character v : s.toCharArray()) {		
				if(vowels.contains(v)) {
					Integer count = vowelMap.get(v);
					vowelMap.put(v, count == null ? 1 : count + 1);
					allVowels++; 
				}
			}
		}

		System.out.println("Vowels: " + vowelMap);	
		System.out.println("Total vowels: " + allVowels);
	}		
}
