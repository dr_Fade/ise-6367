// Create a class, then make an initialized array of objects of your class
// Fill a List from your array. Create a subset of your List by using 
// subList(), then remove this subset from your List.
import java.util.*;

class Class {
	public static int counter = 0;
	private int id = counter++;
	public String toString() { return String.valueOf(id); }	
}

public class ex7 {
	public static void main(String[] args) {		
		Class[] t = new Class[10];
		for(int i = 0; i < t.length; i++)
			t[i] = new Class();
		List<Class> lt = new ArrayList<Class>();

		for(Class x : t) 
			lt.add(x);
		System.out.println("list of Class: " + lt);

		List<Class> sub = lt.subList(2, 6);
		System.out.println("subList: " + sub);

		List<Class> copy = new ArrayList<Class>(lt);
		copy.removeAll(sub);
		
		System.out.println("copy: " + copy);
		lt = copy;
		System.out.println("list of Class: " + lt);
	}
}