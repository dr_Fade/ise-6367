// Modify innerclasses/Sequence.java so that Sequence works with an Iterator
// instead of a Selector.
import java.util.*;

class Sequence {
	private ArrayList<Object> items = new ArrayList<Object>();
	public void add(Object x) {
		items.add(x);
	}
	public Iterator iterator() {
		return items.iterator();
	}
}

public class ex9 {
	public static void main(String[] args) {
		Sequence sequence = new Sequence();
		for(int i = 0; i < 10; i++)
			sequence.add(Integer.toString(i));
		Iterator it = sequence.iterator();
		while(it.hasNext())	System.out.println(it.next());
	}
}





