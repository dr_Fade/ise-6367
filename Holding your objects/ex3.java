// Modify innerclasses/Sequence.java so that you can add any number
// of elements to it.
import java.util.*;

interface Selector {
	boolean end();
	Object current();
	void next();
}

class Sequence {
	private ArrayList<Object> items = new ArrayList<Object>();
	
	public void add(Object x) {
		items.add(x);
	}

	public Selector selector() {
		return new SequenceSelector();
	}

	class SequenceSelector implements Selector {
		private int i = 0;
		public boolean end() {
			return i == items.size();
		}
		public Object current() {
			return items.get(i);
		}
		public void next() {
			i++;
		}
	}
}



public class ex3 {
	public static void main(String[] args) {
		Sequence s3 = new Sequence();
		for(int i = 0; i < 10; i++)
			s3.add(i);
		Selector selector = s3.selector();
		while(!selector.end()) {
			System.out.print(selector.current() + " ");
			selector.next();
		}
		s3.add(0);
		s3.add(3.14);
		s3.add("hello");
		while(!selector.end()) {
			System.out.print(selector.current() + " ");
			selector.next();
		}
	}
}